# Unpacking


# 0   1   2   3
score_list = [80, 95, 30, 78]

first_score, *rest_of_scores = score_list
print(first_score, rest_of_scores)


print(list(enumerate(score_list)))

for index, score in enumerate(score_list):
    print(index, score)

# artist_names = ["Van Gogh", "DaVinci"]

# for name in sorted(artist_names):
#     print(name)

# text = ""
# while text != "stop":
#     text = input("Tell me to stop!")


# lst = ["apple", "orange", "banana", "apple"]

# target_word = "apple"
# count = 0

# for word in lst:
#     if word == target_word:
#         count += 1

# print(count)

# A dictionary is a value that is an unordered collection of key-value pairs
# (Note, since python 3.6 dictionaries retain their order based on how things
# were inserted)


# {"key": value} is a "dictionary literal"

# You can create an empty dictionary with {}

# creates a new dictionary object with the supplied key-value pairs

{"language": "Python"}


me = {
    "first_name": "Bart",
    "last_name": "Dorsey",
    "occupation": "Instructor",
}

jay = {
    "last_name": "Wilson",
    "first_name": "Jay",
    "occupation": "Instructor",
}

terra = {"last_name": "Taylor", "first_name": "Terra", "occupation": "Instructor"}

# .get for a key that doesn't exist, returns None
print(terra.get("firstname"))

# dict["key"] for a key that does exist, raises a KeyError
# print(terra["firstname"])

# Gives us a list of all the keys
print(me.keys())

# Gives us a list of all the values
print(me.values())

# We can loop through a list with for
# By default this gives us the keys

for key in me:
    print(key)

# This is like enumerate but dictionaries
# keys an values
print(me.items())

# you can use items and unpack the key and value
for k, v in me.items():
    print(k, v)
